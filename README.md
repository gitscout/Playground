# Welcome on Gitscout's Playground

The goal of this Playground is to allow our users to try our features or reproduce bugs on a test repo.

Use it as much as you can to try, destroy, experiment stuff with Gitscout. This place is open, just respect the [Code of Conduct](http://contributor-covenant.org/version/1/2/0/).

Have fun and thanks for using Gitscout.

Cheers,
The Gitscout team
